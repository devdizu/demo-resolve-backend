let express = require('express');
let atob = require('atob');
let router = express.Router();

const mailgun = require("mailgun-js");
const mg = mailgun({
	apiKey: "key-3a5d5649f7167ebf1eeac3ece3933912", 
	domain: "sandbox522ac06b7d5b41bc89a57556840773aa.mailgun.org"
});


//Login con Basic Auth
router.post('/', function(req, res, next) {
	
	let auth = {
		username: atob( req.headers.authorization.split(" ")[1] ).split(":")[0],
		password: atob( req.headers.authorization.split(" ")[1] ).split(":")[1]
	}

	//Primero se comprueba si el usuario existe.
	const sql_user_exist = `SELECT * from USER WHERE username = '${ auth.username }'`;
	connection.query(sql_user_exist, function (error, results, fields) {

	  	if(error){
	  		res.status(500).send(error);
	  	} else {
			//Validacion si existe algun registro en la BD
	  		if (results.length == 0) {
				res.status(401).send({
					status: `error`, 
					message: `Nombre de Usuario y/o contraseña incorrectos.`
				});
	  		}
	  		else{
				const [user] = results;
	  			//Luego se valida que la contrasena sea la correcta.
	  			if (auth.password != user.password) {
	  				res.status(401).send({
						status: `error`, 
						message: `Nombre de Usuario y/o contraseña incorrectos.`
					});
	  			}
	  			else{
					//Se genera un token de autenticacion
					delete user.password;
					user.token = Buffer.from(`${ user.username }${ new Date().getTime() }${ Math.random() * 1024 }`).toString('base64');
					
					const sql_save_token = `UPDATE USER set token ='${ user.token }' WHERE id=${ user.id }`;
					connection.query(sql_save_token, function (error, results, fields) {
						if(error){
							res.status(500).send({
								status: `error`, 
								message: error
							});
						} else {
							res.send({
								status: `success`, 
								message: user
							});
						}
					});
	  			}
	  		}
	  	}
  	});

});


//Recuperacion de contrasena
router.post('/recover', function(req, res, next) {
	//res.send("Recover Field");
	const code = Buffer.from(`${req.body.email} ${new Date().getTime() }`).toString('base64');

	const sql_user_exist = `INSERT INTO RECOVER (code, expires, user_id) SELECT '${ code }', DATE_ADD(now(), INTERVAL 1 DAY),  id from USER WHERE email = '${ req.body.email }'`;
	connection.query(sql_user_exist, function (error, results, fields) {
		
		if(error){
			res.status(500).send({
				status: `error`, 
				message: error
			});
		} else {
			//Se valida si inserto un nuevo codigo para recuperar contraseña, si no inserta es porque el usuario no existe
			if(results.affectedRows > 0){
				
				//Envio de codigo por correo
				const emailTemplate = {
					from: 'Resolve Demo <me@samples.mailgun.org>',
					to: req.body.email,
					subject: 'Tu Código para reestablecer la contraseña',
					text: `El Código para reestablecer tu contraseña es: ${code}`,
					html: `El Código para reestablecer tu contraseña es: <a href="http://localhost:4200/recover/${code}" target="_blank">${code}</a>`
				};
				mg.messages().send(emailTemplate, function (error, body) {
					if(error){
						res.status(500).send({
							status: `error`, 
							message: error
						});
					}
					else{
						res.send({
							status: `success`, 
							message: "Codigo Enviado"
						});
					}
				});
				
			}else{
				res.status(401).send({
					status: `error`, 
					message: "No existe ninguna cuenta con este correo"
				});
			}
		}
	});
});

//Recuperacion de contrasena
router.post('/restore-password', function(req, res, next) {
	
});

module.exports = router;