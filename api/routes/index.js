var express = require('express');
var router = express.Router();

//Mensaje de Bienvenida
router.get('/', function(res) {
	res.send({ message: "API Working" });
});

module.exports = router;