DROP DATABASE IF EXISTS db_resolve;
CREATE DATABASE db_resolve;
USE db_resolve;

DROP TABLE IF EXISTS COMPANY;
DROP TABLE IF EXISTS RECOVER;
DROP TABLE IF EXISTS USER;
DROP TABLE IF EXISTS ROLE;


CREATE TABLE ROLE (
    id                         INT NOT NULL AUTO_INCREMENT,
    description                VARCHAR(250)  NOT NULL,
    created_at                 DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ( id )
);
INSERT INTO ROLE(`description`) VALUES ("superadmin");
INSERT INTO ROLE(`description`) VALUES ("admin");

CREATE TABLE USER (
    id                         INT NOT NULL AUTO_INCREMENT,
    firstname                  VARCHAR(100) NOT NULL,
    lastname                   VARCHAR(100) NOT NULL,
    username                   VARCHAR(15) NOT NULL UNIQUE,
    email                      VARCHAR(20) NOT NULL UNIQUE,
    password                   VARCHAR(20) NOT NULL,
    phone                      INT(10) NOT NULL,
    birth                      DATE NOT NULL,
    photo                      BINARY,
    token                      VARCHAR(200),
    created_at                 DATETIME DEFAULT CURRENT_TIMESTAMP,
    role_id                    INT NOT NULL,
    PRIMARY KEY ( id ),
    FOREIGN KEY (role_id) REFERENCES ROLE(id)
);
INSERT INTO USER(`firstname`, `lastname`, `username`, `email`, `password`, `phone`, `birth`, `photo`, `token`, `role_id`) 
            VALUES ("superAdmin","superAdmin","superAdmin","diegojz150@gmail.com","123456",123456789,"1990-01-01", null,null,1);

INSERT INTO USER(`firstname`, `lastname`, `username`, `email`, `password`, `phone`, `birth`, `photo`, `token`, `role_id`) 
            VALUES ("admin","admin","admin","devdizu@gmail.com","123456",123456789,"1990-01-01",null, null,2);

CREATE TABLE RECOVER (
    id                         INT NOT NULL AUTO_INCREMENT,
    code                       VARCHAR(200) NOT NULL,
    created_at                 DATETIME DEFAULT CURRENT_TIMESTAMP,
    expires                    DATETIME NOT NULL,
    user_id                    INT NOT NULL,
    PRIMARY KEY ( id ),
    FOREIGN KEY (user_id) REFERENCES USER(id)
);

CREATE TABLE COMPANY (
    id                         INT NOT NULL AUTO_INCREMENT,
    name                       VARCHAR(50) NOT NULL,
    address                    VARCHAR(50) NOT NULL,
    phone                      INT(10) NOT NULL,
    logo                       BINARY,
    created_at                 DATETIME DEFAULT CURRENT_TIMESTAMP,
    user_id                    INT NOT NULL,
    PRIMARY KEY ( id ),
    FOREIGN KEY (user_id) REFERENCES USER(id)
);
INSERT INTO COMPANY(`name`, `address`, `phone`, `logo`, `user_id`) VALUES ("La Empresa S.A.S.", "Calle 25A # 30-20", 123456789, null, 2);